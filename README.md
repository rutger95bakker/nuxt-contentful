# nuxt-contentful

> Nuxt.js project with Contentful CMS

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev # Or yarn dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project for Netlify
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

### Dependencies

[Nuxt.js docs](https://github.com/nuxt/nuxt.js)
[Vue.js docs](https://github.com/vuejs/vue/)
[Contentful](https://github.com/contentful)
